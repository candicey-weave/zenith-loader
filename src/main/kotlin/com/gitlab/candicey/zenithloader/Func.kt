package com.gitlab.candicey.zenithloader

import java.io.File
import java.lang.instrument.Instrumentation
import java.net.URL
import java.util.jar.JarFile

internal fun info(message: String) = println("[Zenith Loader] $message")

internal fun warn(message: String) = println("[Zenith Loader] [WARN] $message")

internal fun fatal(message: String) = println("[Zenith Loader] [FATAL] $message")

fun appendToClassLoader(file: File, instrumentation: Instrumentation? = null) {
    if (mainClassLoader.javaClass.interfaces.all { it.name != "net.weavemc.loader.bootstrap.transformer.URLClassLoaderAccessor" }) {
        warn("Failed to append to class loader: mainClassLoader (${mainClassLoader.javaClass.name}) does not implement URLClassLoaderAccessor")
    } else {
        val addWeaveURL = mainClassLoader.javaClass.getDeclaredMethod("addWeaveURL", URL::class.java)
        addWeaveURL(mainClassLoader, file.toURI().toURL())

        info("Appended ${file.name} to classloader (${mainClassLoader.javaClass.name})")
    }

    try {
        instrumentation?.appendToSystemClassLoaderSearch(JarFile(file))
    } catch (e: Exception) {
        warn("Failed to append to system class loader search: $e")
    }
}

fun listMods(): List<File> {
    val fileManagerClass = mainClassLoader.loadClass("net.weavemc.loader.util.FileManager")
    val fileManager = fileManagerClass.getDeclaredField("INSTANCE")[null]

    val getMods = fileManagerClass.getDeclaredMethod("getMods")
    val modJars = getMods(fileManager) as List<*>

    val getFile = modJars.firstOrNull()?.javaClass?.getDeclaredMethod("getFile") ?: return emptyList()
    return modJars.map { getFile(it) as File }
}

fun compareVersion(version1: String, version2: String): Int {
    val version1Parts = version1.split('.')
    val version2Parts = version2.split('.')

    for (i in 0 ..< maxOf(version1Parts.size, version2Parts.size)) {
        val part1 = version1Parts.getOrNull(i)?.toIntOrNull() ?: 0
        val part2 = version2Parts.getOrNull(i)?.toIntOrNull() ?: 0

        if (part1 != part2) {
            return part1.compareTo(part2)
        }
    }

    return 0
}