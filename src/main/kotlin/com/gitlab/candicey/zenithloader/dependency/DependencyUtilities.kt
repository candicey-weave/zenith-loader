package com.gitlab.candicey.zenithloader.dependency

import com.gitlab.candicey.zenithloader.*
import com.gitlab.candicey.zenithloader.dependency.Dependency.Companion.toCheckResult
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.VersionJson
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.VersionJson.DependencyJson
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.VersionJson.DependencyJson.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.io.File
import java.net.URL
import java.util.jar.JarFile

typealias Version = String

typealias Versioning = (version: Version) -> String

typealias DependencyFactory = (version: Version) -> Dependency

typealias DependencyJsonConfigurationBased = (VersionJson) -> List<Dependency>

infix fun DependencyFactory.version(version: Version): Dependency = this(version)

infix fun DependencyJsonConfigurationBased.config(json: VersionJson): List<Dependency> = this(json)

fun readConfig(configName: String): List<Dependency> = DependencyUtilities.readConfig(configName)

object DependencyUtilities {
    val versionJsonConfigCache = mutableMapOf<String, VersionJson>()

    fun readConfig(configName: String): List<Dependency> {
        val json = readVersionJson(configName)

        return listDependencies(json)
            .map { it.processor?.config(json) ?: error("No processor found for ${it.name}") }
            .flatten()
            .toList()
    }

    fun readConfig(versionJson: VersionJson): List<Dependency> =
        listDependencies(versionJson)
            .map { it.processor?.config(versionJson) ?: error("No processor found for ${it.name}") }
            .flatten()
            .toList()

    fun listDependencies(json: VersionJson): List<DependencyJson> =
        VersionJson::class.java.declaredFields
            .filter { it.isAnnotationPresent(AnnoDependency::class.java) }
            .mapNotNull { it.also { it.isAccessible = true }[json] }
            .filterIsInstance<DependencyJson>()

    @Target(AnnotationTarget.FIELD)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class AnnoDependency

    @Target(AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class AnnoDependencyName(val name: String)

    @Target(AnnotationTarget.CLASS)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class AnnoDependencyGroupId(val groupId: String)

    @Serializable
    open class VersionJson {
        @AnnoDependency
        @SerialName("Zenith-Core")
        val zenithCore: ZenithCoreJson? = null

        @AnnoDependency
        @SerialName("Concentra")
        val concentra: ConcentraJson? = null

        @Serializable
        @SerialName("Altera")
        val altera: AlteraJson? = null

        @Serializable
        abstract class DependencyJson {
            abstract val version: String?

            abstract val processor: DependencyJsonConfigurationBased?

            abstract val initiator: Initiator?

            open val name: String by lazy {
                javaClass.getAnnotation(AnnoDependencyName::class.java)?.name
                    ?: error("No AnnoDependencyName annotation found for ${javaClass.simpleName}")
            }

            open val groupId: String by lazy {
                javaClass.getAnnotation(AnnoDependencyGroupId::class.java)?.groupId
                    ?: error("No AnnoDependencyGroupId annotation found for ${javaClass.simpleName}")
            }

            @Serializable
            @AnnoDependencyName("Zenith-Core")
            @AnnoDependencyGroupId("com.gitlab.candicey.zenithcore")
            class ZenithCoreJson(
                override val version: String?,
                val versionedAPIs: List<String> = listOf(),
            ) : DependencyJson() {
                override val processor = Dependencies.ZENITH_CORE
                override val initiator = Initiator("com.gitlab.candicey.zenithcore_v${version?.replace('.', '_')}.ZenithCoreBootstrap")
            }

            @Serializable
            @AnnoDependencyName("Concentra")
            @AnnoDependencyGroupId("com.gitlab.candicey.concentra")
            class ConcentraJson(
                override val version: String?,
            ) : DependencyJson() {
                override val processor = Dependencies.CONCENTRA
                override val initiator = Initiator("com.gitlab.candicey.concentra.ConcentraBootstrap", cancel = true)
            }

            @Serializable
            @AnnoDependencyName("Altera")
            @AnnoDependencyGroupId("com.gitlab.candicey.altera")
            class AlteraJson(
                override val version: String?,
            ) : DependencyJson() {
                override val processor = Dependencies.ALTERA
                override val initiator = Initiator("com.gitlab.candicey.altera_v${version?.replace('.', '_')}.AlteraBootstrap")
            }
        }

        @Serializable
        open class Initiator(
            var bootstrapperClass: String,
            var cancel: Boolean = false,
            var preInitialise: (Initiator.() -> Unit)? = null,
            var cancelPreInitialise: Boolean = false,
            var postInitialise: (Initiator.() -> Unit)? = null,
            var cancelPostInitialise: Boolean = false,
        )
    }

    fun urlBytesProvider(url: String) =
        object : BytesProvider {
            override fun invoke(): ByteArray = URL(url).readBytes()

            override fun toString(): String = "URL: $url"
        }

    fun classExistenceChecker(className: Versioning) =
        { version: Version ->
            runCatching { mainClassLoader.getResource(className(version).replace('.', '/') + ".class") ?: throw RuntimeException() }.isSuccess.toCheckResult()
        }

    fun fieldVersionChecker(ownerClass: Versioning, fieldName: Versioning, fieldValueChecker: (Any?) -> Boolean) =
        { version: Version ->
            runCatching {
                val field = Class.forName(ownerClass(version)).getDeclaredField(fieldName(version))
                field.isAccessible = true
                fieldValueChecker(field.get(null))
            }.isSuccess.toCheckResult()
        }

    fun highestVersionChecker(checker: (String) -> CheckResult, dependencyResolver: (VersionJson) -> DependencyJson?, checkJarName: Boolean) =
        { version: Version ->
            // TODO: ALSO CONSIDER DEPENDENCIES
            val jars = listMods()
            val versionsJsonText = jars
                .map { JarFile(it) }
                .flatMap { jarFile ->
                    jarFile.entries()
                        .toList()
                        .filter { it.name.startsWith(ZENITH_LOADER_DEPENDENCIES_VERSIONS_JSON_PREFIX) }
                        .filter { it.name.endsWith(ZENITH_LOADER_DEPENDENCIES_VERSIONS_JSON_SUFFIX) }
                        .map { jarEntry -> jarFile.getInputStream(jarEntry).bufferedReader().readText() }
                }

            val highestVersion = versionsJsonText
                .mapNotNull { text -> runCatching { JSON.decodeFromString<VersionJson>(text) }.getOrNull() }
                .filter { json -> dependencyResolver(json)?.version != null }
                .highestVersion(dependencyResolver)

            if (version != highestVersion.version) {
                // let other Zenith Loader instance handle this
                info("Found newer version of ${highestVersion.name} v${highestVersion.version}, skipping...")

                CheckResult.SKIPPED
            } else {
                if (checkJarName) {
                    val jarName = Dependency.getJarName(highestVersion.groupId, highestVersion.name, version)
                    val jar = File(DEPENDENCIES_DIRECTORY, jarName)

                    if (!jar.exists()) {
                        CheckResult.NOT_FOUND
                    } else {
                        checker(version)
                    }
                } else {
                    checker(version)
                }
            }
        }

    fun createMavenFileProvider(repoUrl: String, groupId: String, artifactId: String) =
        { fileSuffix: String, version: Version -> "$repoUrl/${groupId.replace('.', '/')}/$artifactId/$version/$artifactId-$version$fileSuffix" }

    fun readVersionJson(configName: String) = versionJsonConfigCache.getOrPut(configName) {
        JSON.decodeFromString(
            javaClass.getResourceAsStream("/$ZENITH_LOADER_DEPENDENCIES_VERSIONS_JSON_PREFIX/$configName$ZENITH_LOADER_DEPENDENCIES_VERSIONS_JSON_SUFFIX")
                ?.bufferedReader()
                ?.readText()
                ?: error("Could not find version json for $configName")
        )
    }

    fun basicDependencyVersioning(
        name: Versioning,
        url: Versioning,
        checker: DependencyChecker,
        downloadedJarName: String? = null,
        dependencies: Map<DependencyFactory, Version> = emptyMap()
    ): DependencyFactory =
        { version: Version ->
            Dependency(
                name = name(version),
                bytesProvider = urlBytesProvider(url(version)),
                version = version,
                downloadedJarName = downloadedJarName,
                checker = checker,
                dependencies = dependencies.map { (factory, version) -> factory(version) },
            )
        }
}