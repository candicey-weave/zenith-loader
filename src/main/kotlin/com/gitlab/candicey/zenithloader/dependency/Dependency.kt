package com.gitlab.candicey.zenithloader.dependency

import com.gitlab.candicey.zenithloader.*
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.VersionJson.Initiator
import kotlinx.serialization.Serializable
import java.io.File

typealias BytesProvider = () -> ByteArray
typealias InitiatorProvider = Dependency.() -> Initiator

typealias DependencyChecker = (version: Version) -> CheckResult

@Serializable
data class Dependency(
    val groupId: String = "",
    val name: String,
    val bytesProvider: BytesProvider,
    val version: String,
    val namespace: String? = null,
    val downloadedJarName: String? = null,
    val checker: DependencyChecker = DefaultChecker,
    val initiator: InitiatorProvider? = null,
    val dependencies: List<Dependency> = emptyList(),
    val late: Boolean = false,
) {
    val downloadedJarNameOrDefault: String by lazy { downloadedJarName ?: getJarName(groupId, name, version) }

    val file: File by lazy { DEPENDENCIES_DIRECTORY.resolve(downloadedJarNameOrDefault) }

    fun tryAppendJarToClassLoader(errorIfNotExist: Boolean = false) {
        if (!late) {
            appendJarToClassLoader(errorIfNotExist)
        }
    }

    fun appendJarToClassLoader(errorIfNotExist: Boolean = false) = runCatching {
        appendToClassLoader(
            file.also { if (!it.exists()) { if (errorIfNotExist) error("Dependency jar not found: $downloadedJarNameOrDefault") else return@runCatching } }
                .let { file -> namespace?.let { runCatching { file.remapJarToTemp(it) }.onFailure { it.printStackTrace() }.getOrNull() } ?: file },
            instrumentation
        )
    }

    private object DefaultChecker : DependencyChecker {
        override fun invoke(version: Version): CheckResult = CheckResult.ERROR
    }

    companion object {
        fun getJarName(groupId: String, name: String, version: String): String = "${groupId.replace('.', '/')}/$name-$version.zenithloader.jar".replace(' ', '-')
        fun Boolean.toCheckResult(): CheckResult = if (this) CheckResult.FOUND else CheckResult.NOT_FOUND
    }
}
