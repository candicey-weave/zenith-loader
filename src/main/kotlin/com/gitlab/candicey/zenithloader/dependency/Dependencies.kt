package com.gitlab.candicey.zenithloader.dependency

import com.gitlab.candicey.zenithloader.compareVersion
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.VersionJson
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.classExistenceChecker
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.createMavenFileProvider
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.fieldVersionChecker
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.highestVersionChecker
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.urlBytesProvider
import com.gitlab.candicey.zenithloader.mappings.NamespaceManager
import com.gitlab.candicey.zenithloader.SYSTEM_PROPERTIES
import com.gitlab.candicey.zenithloader.util.PropertyDelegate
import java.net.URL

object Dependencies {
    val ZENITH_CORE: DependencyJsonConfigurationBased = { json: VersionJson ->
        val zenithCoreVersion = json.zenithCore?.version ?: error("No versions found for Zenith Core")
        val url = createMavenFileProvider("https://gitlab.com/api/v4/projects/45235852/packages/maven", "com.gitlab.candicey.zenithcore", "Zenith-Core")
        val zenithCoreJarUrl = url("-relocated.jar", zenithCoreVersion)

        val versionedAPIs = json.zenithCore.versionedAPIs
        val entryPointPropertyName = "zenithcore.v${zenithCoreVersion.replace('.', '_')}.versionEntryPointClass"
        val versionedDependencies = versionedAPIs.map { versionedName ->
            val property = SYSTEM_PROPERTIES.getOrPut(entryPointPropertyName) { listOf<String>() }
            val entryPoints = if (property is List<*>) property else listOf(property.toString()).also { SYSTEM_PROPERTIES[entryPointPropertyName] = it }
            SYSTEM_PROPERTIES[entryPointPropertyName] = entryPoints.plusElement("com.gitlab.candicey.zenithcore_v${zenithCoreVersion.replace('.', '_')}.versioned.${versionedName.lowercase()}.VersionEntryPoint")

            val versionedUrl = createMavenFileProvider("https://gitlab.com/api/v4/projects/45235852/packages/maven", "com.gitlab.candicey.zenithcore.versioned", versionedName)

            val namespaceUrl = versionedUrl("-namespace.txt", zenithCoreVersion)
            val namespace = NamespaceManager.getNamespaceOrPut(NamespaceManager.Identifier(namespaceUrl), save = true) {
                runCatching {
                    URL(versionedUrl("-namespace.txt", zenithCoreVersion)).readText().trim()
                }.getOrNull()?.let(NamespaceManager::Namespace)
            }?.namespace

            Dependency(
                groupId = "com.gitlab.candicey.zenithcore.versioned",
                name = "Zenith-Core-Versioned-API-${versionedName}",
                bytesProvider = urlBytesProvider(versionedUrl("-relocated.jar", zenithCoreVersion)),
                version = zenithCoreVersion,
                namespace = namespace,
                checker = classExistenceChecker { version -> "com.gitlab.candicey.zenithcore_v${version.replace('.', '_')}.versioned.${versionedName.lowercase()}.VersionEntryPoint" },
            ).also(Dependency::tryAppendJarToClassLoader)
        }

        listOf(
            Dependency(
                groupId = "com.gitlab.candicey.zenithcore",
                name = "Zenith-Core",
                bytesProvider = urlBytesProvider(zenithCoreJarUrl),
                version = zenithCoreVersion,
                checker = classExistenceChecker { version -> "com.gitlab.candicey.zenithcore_v${version.replace('.', '_')}.ZenithCoreMain" },
                initiator = { json.zenithCore.initiator },
            ).also(Dependency::tryAppendJarToClassLoader)
        ) + versionedDependencies
    }

    val CONCENTRA: DependencyJsonConfigurationBased = { json: VersionJson ->
        val url = createMavenFileProvider("https://gitlab.com/api/v4/projects/50853422/packages/maven", "com.gitlab.candicey.concentra", "Concentra")
        val concentraVersion = json.concentra?.version ?: error("No versions found for Concentra")
        val concentraJarUrl = url("-relocated.jar", concentraVersion)

        var version: String? by PropertyDelegate("concentra.version.highest") { null }

        listOf(
            Dependency(
                groupId = "com.gitlab.candicey.concentra",
                name = "Concentra",
                bytesProvider = urlBytesProvider(concentraJarUrl),
                version = concentraVersion,
                checker = highestVersionChecker(
                    checker = fieldVersionChecker(
                        ownerClass = { "com.gitlab.candicey.concentra.VarKt" },
                        fieldName = { "CONCENTRA_VERSION" },
                        fieldValueChecker = { it == concentraVersion }
                    ),
                    dependencyResolver = { versionJson -> versionJson.concentra },
                    checkJarName = true
                ),
                initiator = {
                    json.concentra.initiator.apply {
                        preInitialise = {
                            if (version!! == concentraVersion) {
                                cancel = false
                                this@Dependency.appendJarToClassLoader(true)
                            }
                        }
                    }
                },
                late = true,
            ).also { dependency ->
                version?.let {
                    if (compareVersion(it, concentraVersion) > 0) {
                        return@also
                    }
                }

                version = concentraVersion
            }
        )
    }

    val ALTERA: DependencyJsonConfigurationBased = { json: VersionJson ->
        val url = createMavenFileProvider("https://gitlab.com/api/v4/projects/57337404/packages/maven", "com.gitlab.candicey.altera", "Altera")
        val alteraVersion = json.altera?.version ?: error("No versions found for Altera")
        val alteraJarUrl = url("-relocated.jar", alteraVersion)

        listOf(
            Dependency(
                groupId = "com.gitlab.candicey.altera",
                name = "Altera",
                bytesProvider = urlBytesProvider(alteraJarUrl),
                version = alteraVersion,
                checker = classExistenceChecker { version -> "com.gitlab.candicey.altera_v${version.replace('.', '_')}.AlteraBootstrap" },
            )
        )
    }
}