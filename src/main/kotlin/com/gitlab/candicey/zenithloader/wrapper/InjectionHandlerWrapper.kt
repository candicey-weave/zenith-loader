package com.gitlab.candicey.zenithloader.wrapper

import com.gitlab.candicey.zenithloader.mainClassLoader

object InjectionHandlerWrapper {
    val injectionHandlerClass = mainClassLoader.loadClass("net.weavemc.loader.InjectionHandler")

    val instance by lazy { injectionHandlerClass.getDeclaredField("INSTANCE")[null] }

    val modifiers by lazy { injectionHandlerClass.getDeclaredField("modifiers").apply { isAccessible = true }[instance] as MutableList<Any> }

    fun registerModifier(modifier: Any) {
        modifiers.add(modifier)
    }
}