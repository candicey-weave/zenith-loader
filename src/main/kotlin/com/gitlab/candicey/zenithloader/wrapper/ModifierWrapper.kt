package com.gitlab.candicey.zenithloader.wrapper

class ModifierWrapper(val originalInstance: Any) {
    val originalClass by lazy { originalInstance::class.java }

    val namespace: String
        get() = originalClass.getDeclaredMethod("getNamespace")(originalInstance) as String

    val targets: Set<String>
        get() = originalClass.getDeclaredMethod("getTargets")(originalInstance) as Set<String>
}