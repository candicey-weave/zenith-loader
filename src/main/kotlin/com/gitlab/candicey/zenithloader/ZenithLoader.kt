package com.gitlab.candicey.zenithloader

import com.gitlab.candicey.zenithloader.dependency.Dependency
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.VersionJson.Initiator
import com.gitlab.candicey.zenithloader.dependency.readConfig
import com.gitlab.candicey.zenithloader.util.PropertyDelegate
import java.io.File
import java.lang.instrument.Instrumentation
import java.util.function.Consumer
import javax.swing.JOptionPane

object ZenithLoader {
    private val resolvedDependencies = mutableSetOf<Dependency>()

    private val initiators = mutableSetOf<Initiator>()
    private val lateInitiators = mutableSetOf<Initiator>()

    private val LOADED_CLASSES by PropertyDelegate(property = "zenithloader.loaded-classes", defaultValue = { mutableListOf<String>() })

    @JvmStatic
    fun initialiseLateInitiators() = lateInitiators
        .filterNot(Initiator::cancel)
        .forEach(::initialiseInitiator)

    @JvmStatic
    @JvmOverloads
    fun loadDependencies(
        configName: String,
        inst: Instrumentation? = null,
        initiateDependencies: Boolean = true,
        appendToClassLoader: Boolean = true,
        relaunch: Boolean = false
    ) = loadDependencies(
        dependencies = readConfig(configName),
        inst = inst,
        initiateDependencies = initiateDependencies,
        appendToClassLoader = appendToClassLoader,
        relaunch = relaunch
    )

    @JvmStatic
    @JvmOverloads
    fun loadDependencies(
        dependencies: List<Dependency>,
        inst: Instrumentation? = null,
        initiateDependencies: Boolean = true,
        appendToClassLoader: Boolean = true,
        relaunch: Boolean = false
    ) = loadDependencies(
        dependencies = dependencies.toTypedArray(),
        inst = inst,
        initiateDependencies = initiateDependencies,
        relaunch = relaunch
    )

    @JvmStatic
    @JvmOverloads
    fun loadDependencies(
        vararg dependencies: Dependency?,
        inst: Instrumentation? = null,
        initiateDependencies: Boolean = true,
        appendToClassLoader: Boolean = true,
        relaunch: Boolean = false
    ) {
        instrumentation = inst

        recursiveLoadDependencies(dependencies.filterNotNull(), appendToClassLoader, relaunch)

        if (initiateDependencies) {
            initiators
                .filterNot(Initiator::cancel)
                .forEach(::initialiseInitiator)
        }
    }

    private fun recursiveLoadDependencies(
        dependencies: List<Dependency>,
        appendToClassLoader: Boolean,
        relaunch: Boolean
    ): Boolean {
        var shouldRelaunch = false

        for (dependency in dependencies) {
            val dependentDependencies = dependency.dependencies

            if (dependentDependencies.isNotEmpty()) {
                if (recursiveLoadDependencies(dependentDependencies, appendToClassLoader, false)) {
                    shouldRelaunch = true
                }
            }

            if (resolvedDependencies.contains(dependency)) {
                continue
            }

            val checkResult = dependency.checker(dependency.version)

            when (checkResult) {
                CheckResult.ERROR -> fatal(checkResult.message(dependency))
                else -> info(checkResult.message(dependency))
            }

            if (checkResult == CheckResult.NOT_FOUND) {
                obtainDependency(dependency, appendToClassLoader)
            }

            if (checkResult.relaunch) {
                shouldRelaunch = true
            }

            resolvedDependencies.add(dependency)
            dependency.initiator?.let { (if (dependency.late) lateInitiators else initiators).add(dependency.it()) }
        }

        if (relaunch && shouldRelaunch) {
//            LaunchHelper.relaunch()

            warn("Relaunching has been removed. Please relaunch manually.")
            JOptionPane.showMessageDialog(null, "Relaunching has been removed. Please relaunch manually.", "ZenithLoader", JOptionPane.INFORMATION_MESSAGE)
        }

        return shouldRelaunch
    }

    private fun obtainDependency(dependency: Dependency, appendToClassLoader: Boolean) {
        val bytesProvider = dependency.bytesProvider

        val bytes = runCatching { bytesProvider() }.getOrElse {
            throw IllegalStateException("Failed to obtain bytes for ${dependency.name}", it)
        }

        val dependencyJar = File(DEPENDENCIES_DIRECTORY, dependency.downloadedJarNameOrDefault)

        if (!dependencyJar.exists()) {
            dependencyJar.parentFile?.mkdirs()
            dependencyJar.createNewFile()
        }

        dependencyJar.writeBytes(bytes)

        info("Dependency obtained")
        info(" - Group ID: ${dependency.groupId}")
        info(" - Name: ${dependency.name}")
        info(" - Version: ${dependency.version}")
        info(" - Bytes Provider: ${dependency.bytesProvider}")
        info(" - Path: ${dependencyJar.canonicalPath}")

        if (appendToClassLoader) {
            dependency.tryAppendJarToClassLoader(true)
        }
    }

    private fun initialiseInitiator(initiator: Initiator) {
        if (!initiator.cancelPreInitialise) {
            initiator.preInitialise?.let { call ->
                info("Pre-initialising initiator: $initiator")
                initiator.call()
            }
        }

        if (!initiator.cancel) {
            val bootstrapperClass = initiator.bootstrapperClass

            val isLoaded = isClassLoaded(bootstrapperClass)
            if (!isLoaded) {
                info("Running initiator: $initiator => $bootstrapperClass")

                try {
                    val consumer = mainClassLoader.loadClass(bootstrapperClass).getDeclaredConstructor().newInstance() as Consumer<Instrumentation?>
                    consumer.accept(instrumentation)
                } catch (e: Exception) {
                    warn("Failed to run initiator")
                    e.printStackTrace()
                }

                addLoadedClass(bootstrapperClass)
            }
        }

        if (!initiator.cancelPostInitialise) {
            initiator.postInitialise?.let { call ->
                info("Post-initialising initiator: $initiator")
                initiator.call()
            }
        }
    }

    private fun isClassLoaded(className: String): Boolean = LOADED_CLASSES.contains(className)

    private fun addLoadedClass(className: String) = LOADED_CLASSES.add(className)
}