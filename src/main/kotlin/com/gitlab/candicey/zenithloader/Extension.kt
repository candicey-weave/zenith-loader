package com.gitlab.candicey.zenithloader

import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.VersionJson
import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities.VersionJson.DependencyJson
import com.gitlab.candicey.zenithloader.dependency.Version
import com.gitlab.candicey.zenithloader.wrapper.InjectionHandlerWrapper
import com.gitlab.candicey.zenithloader.wrapper.ModifierWrapper
import java.io.File
import java.security.MessageDigest

val String.sha256: String
    get() = MessageDigest
        .getInstance("SHA-256")
        .digest(toByteArray())
        .joinToString("") { "%02x".format(it) }

fun List<Version>.sorted(): List<Version> = sortedWith { v1, v2 ->
    val v1Split = v1.split('.')
    val v2Split = v2.split('.')

    for (i in 0 ..< v1Split.size.coerceAtMost(v2Split.size)) {
        val v1Part = v1Split[i].toIntOrNull() ?: return@sortedWith 0
        val v2Part = v2Split[i].toIntOrNull() ?: return@sortedWith 0

        if (v1Part != v2Part) {
            return@sortedWith v1Part - v2Part
        }
    }

    return@sortedWith 0
}

fun List<VersionJson>.highestVersion(dependencyResolver: (VersionJson) -> DependencyJson?): DependencyJson =
    associateWith { json -> dependencyResolver(json) }
        .filter { it.value != null && it.value!!.version != null }
        .let {
            val highestVersion = it.values.mapNotNull { json -> json?.version }.sorted().lastOrNull()

            it.entries.find { entry -> entry.value?.version == highestVersion }?.value
                ?: error("No versions found for ${dependencyResolver(firstOrNull() ?: VersionJson())?.name}")
        }

fun File.remapJarToTemp(namespace: String): File {
    val modConfigClass = mainClassLoader.loadClass("net.weavemc.internals.ModConfig")
    // only "namespace" is used, but we need to provide all parameters to the constructor
    val modConfig = modConfigClass.declaredConstructors.first { it.parameterCount == 9 }.newInstance(
        /* name */ "ZenithLoader-Dependencies-${nameWithoutExtension}",
        /* modId */ "zenithloader_dependencies-${nameWithoutExtension}",
        /* entryPoints */ emptyList<String>(),
        /* mixinConfigs */ emptyList<String>(),
        /* hooks */ emptyList<String>(),
        /* tweakers */ emptyList<String>(),
        /* namespace */ namespace,
        /* dependencies */ emptyList<String>(),
        /* compiledFor */ "version"
    )

    val weaveLoaderKt = mainClassLoader.loadClass("net.weavemc.loader.util.UtilityKt")
    val createRemappedTemp = weaveLoaderKt.getDeclaredMethod("createRemappedTemp", File::class.java, String::class.java, modConfigClass)

    return createRemappedTemp(null, this, "zenithloader_dependencies-", modConfig) as File
}

fun File.registerMixinConfigs(namespace: String, modId: String, mixinConfigs: List<String>) {
    val weaveLoader = mainClassLoader.loadClass("net.weavemc.loader.WeaveLoader")
    val weaveLoaderInstance = weaveLoader.declaredMethods.first { it.name == "getInstance" }(null)

    val mixinForNamespace = weaveLoader.declaredMethods.first { it.name == "mixinForNamespace" }.also { it.isAccessible = true }
    val mixinLoader = mixinForNamespace(weaveLoaderInstance, namespace)

    val state = mixinLoader::class.java.declaredMethods.first { it.name == "getState" }(mixinLoader)
    val registerMixin = state::class.java.declaredMethods.first { it.name == "registerMixin" } // modId: String, resourceName: String

    for (mixinConfig in mixinConfigs) {
        registerMixin(state, modId, mixinConfig)
        info("Registered mixin config $mixinConfig for $modId")
    }

    val mixinAccessImpl = state::class.java.declaredFields.first { it.type.name == "net.weavemc.loader.mixin.MixinAccess" }.also { it.isAccessible = true }[state]
    val hasForcedSelect = mixinAccessImpl::class.java.getDeclaredField("hasForcedSelect").also { it.isAccessible = true }
    hasForcedSelect[null] = false

    val transformer = state::class.java.getDeclaredField("transformer").apply { isAccessible = true }[state]
    val targets = state::class.java.declaredMethods.first { it.name == "findTargets" }(state, transformer) as Set<String>
    if (targets.isEmpty()) {
        return
    }

    val mappingsHandler = mainClassLoader.loadClass("net.weavemc.loader.util.MappingsHandler").getDeclaredField("INSTANCE")[null]
    val environmentNamespace = mappingsHandler::class.java.declaredMethods.first { it.name == "getEnvironmentNamespace" }(mappingsHandler) as String
    val mapper = mappingsHandler::class.java.getDeclaredMethod("mapper", String::class.java, String::class.java)(mappingsHandler, namespace, environmentNamespace)

    val anonymousModifier = mainClassLoader.loadClass("net.weavemc.loader.WeaveLoader\$populateMixinModifiers\$1")

    val modifiers = InjectionHandlerWrapper
        .modifiers
        .filter { it::class.java == anonymousModifier }
        .map { ModifierWrapper(it) }
        .find { it.namespace == namespace }

    if (modifiers == null) {
        val newModifier = anonymousModifier
            .declaredConstructors
            .first { it.parameterCount == 4 }
            .also { it.isAccessible = true }
            .newInstance(namespace, targets, mapper, state)

        InjectionHandlerWrapper.registerModifier(newModifier)

        info("Populated mixin modifiers for $namespace (new)")
    } else {
        val map = mapper::class.java.declaredMethods.first { it.name == "map" }
        fun map(target: String): String = map(mapper, target.replace('.', '/')) as String

        val newTargets = targets.map(::map)

        (modifiers.targets as HashSet<String>).addAll(newTargets)

        info("Populated mixin modifiers for $namespace (existing)")
    }
}
