package com.gitlab.candicey.zenithloader.parser

import com.gitlab.candicey.zenithloader.*
import com.google.gson.annotations.SerializedName
import java.io.File
import java.net.URL

object ModuleParser {
    val cacheDirectory = File(ZENITH_LOADER_DIRECTORY, "cache-modules").also(File::mkdirs)

    fun parseFromJson(json: String): Module = GSON.fromJson(json, Module::class.java)

    fun parseFromFile(file: File): Module = parseFromJson(file.readText())

    fun parseFromUrl(url: String): Module = parseFromJson(URL(url).readText())

    fun getCachedFile(url: String): File = File(cacheDirectory, "${url.sha256}.json")

    fun getCachedModule(url: String): Module? {
        val cacheFile = getCachedFile(url)

        if (!cacheFile.exists()) {
            return null
        }


        val cacheModules = runCatching { GSON.fromJson(cacheFile.readText(), Module::class.java) }.getOrNull()
        if (cacheModules == null) {
            warn("Failed to parse cache modules from ${cacheFile.canonicalPath} (url: $url)")
            return null
        }

        return cacheModules
    }

    fun getCachedOrFetchModule(url: String): Module {
        val cachedModule = getCachedModule(url)
        if (cachedModule != null) {
            return cachedModule
        }

        info("Fetching module from $url")

        val cacheFile = getCachedFile(url).apply {
            delete()
            createNewFile()
        }
        val module = parseFromUrl(url)
        cacheFile.writeText(GSON.toJson(module))

        return module
    }

    data class Module(
        @SerializedName("formatVersion") val formatVersion: String,
        @SerializedName("component") val component: Component,
        @SerializedName("createdBy") val createdBy: CreatedBy,
        @SerializedName("variants") val variants: List<Variant>,
    ) {
        data class Component(
            @SerializedName("group") val group: String,
            @SerializedName("module") val module: String,
            @SerializedName("version") val version: String,
            @SerializedName("attributes") val attributes: Attributes,
        )

        data class Attributes(
            @SerializedName("org.gradle.status") val orgGradleStatus: String,
        )

        data class CreatedBy(
            @SerializedName("gradle") val gradle: Gradle,
        )

        data class Gradle(
            @SerializedName("version") val version: String,
        )

        data class Variant(
            @SerializedName("name") val name: String,
            @SerializedName("attributes") val attributes: Attributes,
            @SerializedName("dependencies") val dependencies: List<Dependency>,
            @SerializedName("files") val files: List<File>,
        )

        data class Dependency(
            @SerializedName("group") val group: String,
            @SerializedName("module") val module: String,
            @SerializedName("version") val version: Version,
        )

        data class Version(
            @SerializedName("requires") val requires: String,
        )

        data class File(
            @SerializedName("name") val name: String,
            @SerializedName("url") val url: String,
            @SerializedName("size") val size: Long,
            @SerializedName("sha512") val sha512: String,
            @SerializedName("sha256") val sha256: String,
            @SerializedName("sha1") val sha1: String,
            @SerializedName("md5") val md5: String,
        )
    }
}