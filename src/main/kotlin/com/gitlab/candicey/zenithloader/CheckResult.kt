package com.gitlab.candicey.zenithloader

import com.gitlab.candicey.zenithloader.dependency.Dependency

enum class CheckResult(val message: (Dependency) -> String, var relaunch: Boolean = false) {
    FOUND({ "Found ${it.name} v${it.version}" }),
    NOT_FOUND({ "${it.name} v${it.version} not found, downloading..." }, true),
    SKIPPED({ "Skipped ${it.name} v${it.version}" }),
    ERROR({ "Error while checking ${it.name} v${it.version}" }, true);

    companion object {
        fun fromBoolean(boolean: Boolean): CheckResult = if (boolean) FOUND else NOT_FOUND
    }
}