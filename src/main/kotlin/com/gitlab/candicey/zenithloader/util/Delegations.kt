package com.gitlab.candicey.zenithloader.util

import com.gitlab.candicey.zenithloader.SYSTEM_PROPERTIES
import kotlin.reflect.KProperty

/**
 * A delegate that allows you to delegate to a System property.
 *
 * This delegate uses the `System.getProperties` method to retrieve the value of a property.
 * The property name is determined by the variable name where this delegate is used.
 *
 * If the property does not exist, it will be created with the default value provided.
 */
class PropertyDelegate<T>(private val property: String? = null, private val defaultValue: (() -> T)? = null) {
    operator fun getValue(instance: Any, kProperty: KProperty<*>): T {
        val propertyName = property ?: kProperty.name

        return SYSTEM_PROPERTIES.getOrPut(propertyName) { (defaultValue ?: error("Property $propertyName does not exist, and no default value was provided.")).invoke() } as T
    }

    operator fun getValue(instance: Nothing?, kProperty: KProperty<*>): T {
        val propertyName = property ?: kProperty.name

        return SYSTEM_PROPERTIES.getOrPut(propertyName) { (defaultValue ?: error("Property $propertyName does not exist, and no default value was provided.")).invoke() } as T
    }

    operator fun setValue(instance: Any, kProperty: KProperty<*>, value: T) {
        val propertyName = property ?: kProperty.name

        SYSTEM_PROPERTIES[propertyName] = value
    }

    operator fun setValue(instance: Nothing?, kProperty: KProperty<*>, value: T) {
        val propertyName = property ?: kProperty.name

        SYSTEM_PROPERTIES[propertyName] = value
    }
}