package com.gitlab.candicey.zenithloader

import com.gitlab.candicey.zenithloader.dependency.DependencyUtilities
import com.google.gson.GsonBuilder
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass
import java.io.File
import java.lang.instrument.Instrumentation

val WEAVE_DIRECTORY = File(System.getProperty("user.home"), ".weave").also(File::mkdirs)
val MODS_DIRECTORY = File(WEAVE_DIRECTORY, "mods").also(File::mkdirs)
val ZENITH_LOADER_DIRECTORY = File(WEAVE_DIRECTORY, "Zenith-Loader").also(File::mkdirs)
val DEPENDENCIES_DIRECTORY = File(ZENITH_LOADER_DIRECTORY, "dependencies").also(File::mkdirs)

const val ZENITH_LOADER_DEPENDENCIES_VERSIONS_JSON_PREFIX = "zenithloader/dependencies"
const val ZENITH_LOADER_DEPENDENCIES_VERSIONS_JSON_SUFFIX = ".versions.json"

internal val JSON = Json {
    ignoreUnknownKeys = true
    prettyPrint = true
    serializersModule = SerializersModule {
        polymorphic(DependencyUtilities.VersionJson.DependencyJson::class) {
            subclass(DependencyUtilities.VersionJson.DependencyJson.ConcentraJson::class)
            subclass(DependencyUtilities.VersionJson.DependencyJson.ZenithCoreJson::class)
        }
    }
}

internal val GSON = GsonBuilder().setPrettyPrinting().create()

val SYSTEM_PROPERTIES get() = System.getProperties()

val mainClassLoader = ZenithLoader::class.java.classLoader!!

var instrumentation: Instrumentation? = null
    internal set