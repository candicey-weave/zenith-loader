package com.gitlab.candicey.zenithloader.mappings

import com.gitlab.candicey.zenithloader.JSON
import com.gitlab.candicey.zenithloader.ZENITH_LOADER_DIRECTORY
import com.gitlab.candicey.zenithloader.mappings.NamespaceManager.namespaceCacheFile
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import java.io.File

typealias NamespaceCache = MutableMap<NamespaceManager.Identifier, NamespaceManager.Namespace?>

object NamespaceManager {
    val namespaceCacheFile by lazy {
        File(ZENITH_LOADER_DIRECTORY, "namespace-cache.json").apply {
            parentFile?.mkdirs()
            if (createNewFile()) {
                writeText("{}")
            }
        }
    }

    val namespaceCache by lazy { JSON.decodeFromString<NamespaceCache>(namespaceCacheFile.readText()) }

    fun getNamespace(identifier: Identifier): Namespace? = namespaceCache[identifier]

    fun getNamespaceOrPut(identifier: Identifier, save: Boolean = false, namespace: () -> Namespace?): Namespace? =
        try {
            namespaceCache.getOrPut(identifier) { namespace() }
        } finally {
            if (save) {
                save()
            }
        }

    fun save() = namespaceCacheFile.writeText(JSON.encodeToString(namespaceCache))

    @Serializable
    @JvmInline
    value class Identifier(val id: String)

    @Serializable
    @JvmInline
    value class Namespace(val namespace: String)
}