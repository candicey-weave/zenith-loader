plugins {
    kotlin("jvm") version ("1.9.0")
    `maven-publish`
    kotlin("plugin.serialization") version ("1.9.0")
}

val projectGroup: String by project
val projectVersion: String by project
val gitlabProjectId: String by project

group = projectGroup
version = projectVersion

repositories {
    mavenCentral()

    maven {
        url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
        name = "GitLab"
        credentials(HttpHeaderCredentials::class) {
            name = "Private-Token"
            value = findProperty("gitLabPrivateToken") as String?
        }
        authentication {
            create("header", HttpHeaderAuthentication::class)
        }
    }
}

dependencies {
    testImplementation(kotlin("test"))

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.5.1")
    implementation("com.google.code.gson:gson:2.10.1")
}

java {
    withSourcesJar()
    withJavadocJar()
}

kotlin {
    jvmToolchain(8)
}

tasks.test {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

val cleanBuild: Task = tasks.create("cleanBuild") {
    dependsOn(tasks.getByName("clean"))
    finalizedBy(tasks.getByName("build"))
}